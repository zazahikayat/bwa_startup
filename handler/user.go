package handler

import (
	"bwastartup/helper"
	"bwastartup/user"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type userHandler struct {
	service user.Service
}

func NewUserHandler(service user.Service) *userHandler {
	return &userHandler{service}
}

func (h *userHandler) RegisterUser(c *gin.Context) {
	var input user.RegisterUserInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		log.Fatal("error bind json => ", err.Error())
	}
	user, _ := h.service.RegisterUser(input)
	helper.APIResponse("Register user", http.StatusOK, "success", gin.H{
		"id":    user.ID,
		"email": user.Email,
		"name":  user.Name,
	})
}

// alur flow aplikasi
// -> handler akan menhandler inputan user dan di map ke struct
// -> handler akan meneruskan ke service
// -> service akan di teruskan ke ropository
// -> repository bertugas untuk meyimpan data ke database
