package user

import (
	"log"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type Service interface {
	RegisterUser(input RegisterUserInput) (User, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) RegisterUser(input RegisterUserInput) (User, error) {
	var user User
	user.Name = input.Name
	user.Email = input.Email
	password, err := bcrypt.GenerateFromPassword([]byte(input.Password), 10)
	if err != nil {
		log.Fatal("error password hash => ", err.Error())
	}
	user.PasswordHash = string(password)
	user.CreatedAt = time.Now()
	newUser, err := s.repository.Save(user)
	if err != nil {
		log.Fatal("error save user => ", err.Error())
	}
	return newUser, nil
}
